#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#16/03/23
#Ivan Pardo
#Validar que es rep un argument i que es un director i llistar-ne el conjunt
#---------------------------------------------------------

if [ $# -ne 1 ];then
  echo "ERROR:Nums arg incorrecte"
  echo "Usage: $0 llistar-directori"
  exit 1
fi

dir=$1

if ! [ -d $dir ];then
  echo "ERROR: $dir no es un directori"
  echo "Usage: $0 dir"
  exit 2
fi

llista=$(ls $dir)

for elem in $llista
do
   if [ -d $dir/$elem ];then
    echo "$elem -- directori"
   elif [ -f $dir/$elem ];then
     echo "$elem -- regular file"
   elif [ -L $dir/$elem  ];then
     echo "$elem -- Symbolic Link"
   else
     echo "$elem -- Altre"
   fi
done
exit 0

