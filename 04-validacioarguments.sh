#!bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#27/2/23
#Ivan Pardo
#
#Es un programa que rep 2 arguments i els valida
#--------------------------------------------------------------

#1) si num args incorrecte plegar
if [ $# -ne 2 ]
then
  echo "ERROR: numero args incorrecte"
  echo "Usage: $0 nom edat"
  exit 99
fi
#2) Xixa
  echo "nom: $1 edat: $2"
