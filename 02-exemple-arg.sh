#! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#Exemple de primer programa
#Normes:
#   shebang (#!) indica qui ha d'interpretar el fitxer
#   Exemple de procesar arguments 23/2/23 Ivan Pardo
#--------------------------------------------------------------
echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$1: ' $1
echo '$2: ' $2
echo '$9: ' ${10}
echo '$11: '${11}
echo '$$: '$$
nom="puig"
echo "${nom}deworld"
