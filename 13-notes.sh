#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#06/03/23
#Ivan Pardo
#UN programa que et diu si has aprovat,suspes, si has tret un notable o un excelent
#---------------------------------------------------------

for nota in $*
do

  if ! [ $nota -ge 0 -a $nota -le 10 ];then
    echo "ERROR: introdueix una nota correcta [1-10]">>/dev/stderr
  elif [ $nota -lt 5 ];then
    echo "$num: Has tret un $nota. Suspès."
  elif [ $nota -lt 7 ];then
    echo "$num: Has tret un $nota. Aprovat."
  elif [ $nota -lt 9 ];then
    echo "$num: Has tret un $nota. Notable."
   else
     echo "$num: Has tret un $nota. Exelent"
    fi
done
exit 0
