#!/bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#27/2/23
#Ivan Pardo
#Valida si el argument introduit es un directori i fa un ls
#
#
#--------------------------------------------------------------
#Validar si es un argument valid
if [ $# -ne 1 ]
then
  echo "ERROR: Nums args incorrecte"
  echo "usage: $0 validar-directori"
  exit 1
fi
#validar si el argument introduit es un directori
if [ ! -d $1 ]
then
  echo "ERROR: El arg introduit no es un directori"
  echo "Usage: $0 validar-directori"
  exit 2
fi
#XIXA
ls $1
exit 0
