#! /bin/bash
#Març 2023
# @edat ASIX 2023
#
#18-argsfor.sh

opcion=""
arguments=""
myfile=""
num=""

while [ -n "$1" ]
do
  case $1 in
  -[abcd]) opcions="$opcions $1";;
  -f) myfile=$2
    shift;;
  -n) num=$2
     shift;;
  *) arguments="$arguments $1";;
  esac

  shift
done
echo "opcions: $opcions"
echo "arguments: $arguments"
echo "file: $myfile"
echo "num: $num"
