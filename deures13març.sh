#!/bin/bash

#Deures a casa 4)







#1)

for arg in "$@"; do
  grep "^$arg" | cut -c 4
done
exit 0



#10)
num=$1
count=1
while read line && [ $count -le $num ]; do

    echo "$count: $line"
    ((count++))
done

exit 0



#9)

while read -r line; do
    grep -q  "^$line:" /etc/passwd
    if  [ $? -ne 0 ];then
         echo "Error: Usuari no Existeix!"
    else
	 grep "^$line:" /etc/passwd | cut -d: -f 1
    fi
done
exit 0




#8)
for user in "$@"; do
	grep -q "^$user:" /etc/passwd
    if  [ $? -ne 0 ];then
	 echo "Error: Usuari no Existeix!"
    else
   	grep "^$user:" /etc/passwd | cut -d: -f 1
	    
    fi
done
exit 0


#7):
#!/bin/bash

while read -r line; do
    echo "$line" | grep -E '.{61,}'
done
exit 0




#5)
while read -r line; do
echo "$line" | cut -c 1-50
done
exit 0


#3)
cont=0
while [[ $cont -le $1 ]]; do
echo "$cont"
  ((cont++))
done
exit 0

#6)
diaslabo=0
festius=0

for day in "$@"; do
    case $day in
        "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
            ((diaslabo++))
            ;;
        "dissabte"|"diumenge")
            ((festius++))
            ;;
        *)
            echo "Error: $day no és un dia de la setmana" >&2
            exit 1
            ;;
    esac
done

echo "Laborables: $diaslabo"
echo "Festius: $festius"
exit 0

