#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#16/03/23
#Ivan Pardo
#Validar que es rep un argument i que es un director i llistar-ne el conjunt
#---------------------------------------------------------

if [ $# -eq 0 ];then
  echo "ERROR:Nums arg incorrecte"
  echo "Usage: $0 llistar-directori"
  exit 1
fi

for dir in $*
do 
  if ! [ -d $dir ];then
    echo "ERROR: $dir no es un directori" 1>&2
    echo "Usage: $0 dir"
  else
    llista=$(ls $dir)
    echo "-----------Contingut Carpeta:$dir-----------"
    for elem in $llista
    do
       if [ -L $dir/$elem ];then
        echo "$elem -- Symbolic Link"
       elif [ -f $dir/$elem ];then
         echo "$elem -- regular file"
       elif [ -d $dir/$elem  ];then
         echo "$elem -- Directori"
       else
         echo "$elem -- Altre"
       fi
    done
   fi
done
exit 0

