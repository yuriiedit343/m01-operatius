#!bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#23/2/23
#Ivan Pardo
#Exemple if indicar si es major d'edat o menor
#  $prog edat
#
#--------------------------------------------------------------

#1) validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#2) xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat es menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat $edat es edad activa"
else
  echo "edat $edat esta jubilat"
fi
exit 0


