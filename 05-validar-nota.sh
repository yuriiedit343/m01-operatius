#!bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#23/2/23
#Ivan Pardo
#Valida si la nota introduida es aprobat o suspes
#
#
#--------------------------------------------------------------
#Validar nums args !=1
if [ $# -ne 1 ]
then
  echo "ERROR: Nums args incorrecte"
  echo "Usage: $0 nota"
  exit 1
fi
#Validar si la nota es valida (menor de 10)
if [ $1 -gt 10 ]
then
  echo "ERROR: Nota no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit 2
fi
#Dir si la nota es suspes o aprobat
if [ $1 -lt 5 ]
then
  echo "La nota: $1  esta suspesa"
else
  echo "La nota: $1  esta aprovada"
fi
exit 0
