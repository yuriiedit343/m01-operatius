#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#09/03/23
#Ivan Pardo
#Exemples bucle while
#---------------------------------------------------------

#7) numera i mostra en majuscules la entrada estandart
cont=1
while read -r line
do
  echo "$cont)$line" | tr 'a-z' 'A-Z'
  ((cont++))
done
exit 0




#6) mostrar stdin linea a linea fins token FI

read -r line
while [ $line != "Fi" ]
do 
  echo "$line"
  read -r line
done
exit 0






#5) mostrar numerada l'entrada estandart
cont=1
while read -r line
do
  echo "$cont)$line"
  ((cont++))
done
exit 0





#4) processar entrada estandard linia a linea

while read -r line
do
  echo "$line"
done
exit 0




#3) iterar per la llista d'arguments

while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0






#versio 2)comptador decreixen "x" to 0

cont=0
num=$1
while [ $num -ge $cont ]
do
  echo "$num"
  ((num--))
done




exit 0






cont=0
max=10
while [ $cont -le $max ]
do
 echo "$cont"
 ((cont++))
done
exit 0

