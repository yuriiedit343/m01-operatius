#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#06/03/23
#Ivan Pardo
#UN programa que suma
#---------------------------------------------------------
err_nargs=1

#validar arguments
if  [ $# -eq 0 ];then
  echo "ERROR: numero de args incorrectes"
  echo "Usage: $0 num1..."
  exit "err_nargs"
fi

suma=0
for num in $*
do
  suma=$((num+suma))
done
echo "La suma dels valors es: suma"
exit 0
