#!/bin/bash
#edt ASIX-M01
#Febrer 2023
#
#27/2/23
#Ivan Pardo
#Indica quin tipus de file es
#
#
#--------------------------------------------------------------
#Validar si es un argument valid
if [ $# -ne 1 ]
then
  echo "ERROR: Nums args incorrecte"
  echo "usage: $0 validar-directori"
  exit 1
fi
#xixa
if [ ! -e $1 ];then
  echo "$1 no existeix"
elif [ -d $1 ]
then
  echo "Es un directori"
elif [ -h $1 ]
then
  echo "Es un link"
elif [ -f $1 ];then
  echo "Es un regular file"
else
  echo "$1 és un altre cosa"
fi
exit 0
