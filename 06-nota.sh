#!bin/bash
#@edt ASIX-M01
#Febrer 2023
#
#27/2/23
#Ivan Pardo
#Valida si la nota introduida es suspes, aprovat, notable, exelent
#
#
#--------------------------------------------------------------
#Validar nums args !=1
if [ $# -ne 1 ]
then
  echo "ERROR: Nums args incorrecte"
  echo "Usage: $0 nota"
  exit 1
fi
#Validar si la nota es valida (menor de 10)
if [ $1 -gt 10 ]
then
  echo "ERROR: Nota no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit 2
fi
#xixa
if [ $1 -lt 5 ]
then
  echo "La nota: $1  esta suspesa"
elif [ $1 -lt 7 ]
then
  echo "La nota: $1 esta aprovada"

elif [ $1 -lt 9 ]
then
  echo "La nota: $1 es un notable"

else
  echo "La nota: $1  es un exelent"
fi
exit 0
