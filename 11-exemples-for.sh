#!/bin/bash
#edt ASIX-M01
#Març 2023
#
#06/03/23
#Ivan Pardo
#Exemples for
#---------------------------------------------------------

#8) llistar tots els logins numerats
llista_logins=$(cut -d: -f1 /etc/passwd| sort)
cont=1
for login in $llista_logins
do
  echo "$cont:$login"
  ((cont++))
done
exit 0




#7) llista fitxers directori actiu numerats
llista=$(ls)
cont=1
for nom in $llista
do
  echo "$cont:$nom"
  ((cont++))
done
exit 0




#6) numerar els arguments
cont=1
for arg in $*
do
  echo "$cont:$arg"
  cont=$((cont+1))
done 
exit 0






#5) iterar i mostrar la llista d'arguments

for arg in "$*"
do
  echo "$arg"
done
exit 0




#4) iterar i mostrar la llista d'arguments

for arg in $*
do
  echo "$arg"
done 
exit 0




#3) Iterar pel valor d'8na variable
llistat=$(ls)
for nom in $llistat
do
  echo "$nom"
done
exit 0


#2)
for nom in "pere marta pau anna"
do
  echo "$nom"
done
exit 0

#1) iterar per un conjunt d'elements

for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0


